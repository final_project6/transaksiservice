# transaksiService
berisi aplikasi yang menangani service tentang transaksi yang ada pada userService dan sellerService. Dalam service ini dapat digunakan untuk melihat transaksi apa saja yang ada dengan return data lengkap dari transaksi dan data mobil

## Cara Menjalankan
masuk ke direktori kemudian ketikkan `python app.py`, akan berjalan pada port 5001

## Routing API
### Show All Transaksi
`http://localhost:5001/alltransaksi` method GET

### Show Transaksi By Id Transaksi
`http://localhost:5001/transaksi/id` method GET

data = {<br>
    "id_transaksi" : 2 <br>
}

### Insert Data Transaksi
`http://localhost:5001/transaksi/insert` method POST

data = {<br>
    "user_id" : 3, <br>
    "car_id" : "613c96dfd061a55e37571697" <br>
}

### Update Data Transaksi
hanya untuk mengganti id_car yang ada<br>
`http://localhost:5001/transaksi/update` method POST

data = {<br>
    "id_transaksi":3,<br>
    "car_id" : "613c96fcd061a55e37571698"<br>
}

### Delete Transaksi By Id Transaksi
`http://localhost:5001/transaksi/delete` method POST

data = {<br>
    "id_transaksi" : 3<br>
}


