from flask import Flask, jsonify, request
from controllers import transaksi
from flask_jwt_extended import JWTManager

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app.config['JWT_SECRET_KEY'] = "testkunci"
jwt = JWTManager(app)

# route API yang digunakan untuk melihat semua data transaksi
@app.route("/alltransaksi", methods=["GET"])
def transaksiService():
    try:
        return transaksi.viewAllTransaksi()
    except Exception as e:
        print(f"kesalahan API transaksiService: {e}")


# route API yang digunakan untuk melihat data by id_transaksi
@app.route("/transaksi/id", methods=["GET"])
def transaksiByIdService():
    try:
        params = request.json
        return transaksi.viewTransaksiById(**params)
    except Exception as e:
        print(f"kesalahan function transaksiByIdService: {e}")


# route APi yang digunakan untuk menambah data user
@app.route("/transaksi/insert", methods=["POST"])
def insertTransaksiService():
    try:
        params = request.json
        return transaksi.tambahData(**params)
    except Exception as e:
        print(f"kesalahan function inserTransaksiService: {e}")


# route API yang digunakan untuk mengubah data transaksi
@app.route("/transaksi/update", methods=["POST"])
def updateTransaksiService():
    try:
        params = request.json
        return transaksi.ubahDataById(**params)
    except Exception as e:
        message = {
            "message": "kesalahan fucntion updateTransaksiService",
            "error" : e
        }
        return jsonify(message)


# route API yang digunakan untuk menghapus data transaksi berdasararkan id_transaksi
@app.route("/transaksi/delete", methods=["POST"])
def deleteTransaksiService():
    try:
        params = request.json
        return transaksi.hapusDataById(**params)
    except Exception as e:
        message = {
            "message" : "kesalahan fucntion deleteTransaksiService",
            "error" : e
        }
        return jsonify(message)


if __name__ == "__main__":
    app.run(debug=True, port=5001)