from sqlalchemy import create_engine, Column, String, Integer
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Transaksi(Base):
    __tablename__ = "transaksi"
    id_transaksi = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    car_id = Column(String)

class Database:
    # method yang digunakan untuk melakukan koneksi database
    def __init__(self):
        try:
            self.engine = create_engine("mysql+mysqlconnector://root:@localhost:3306/jualmobil", echo=True)
            self.Session = sessionmaker(bind=self.engine)
            self.session = self.Session()
            print("sukses koneksi")
        except Exception as e:
            print(f"gagal koneksi database: {e}")

    # method yang digunakan untuk melihat semua data dalam tabel transaksi
    def showTransaksi(self):
        try:
            hasil = self.session.query(Transaksi).all()
            return hasil  
        except Exception as e:
            print(f"kesalahan function showTransaksi: {e}")

    # method yang digunakan untuk melihat data transaksi berdasarkan id_transaksi
    def showTransaksiById(self, **params):
        try:
            hasil = self.session.query(Transaksi).filter(Transaksi.id_transaksi == params["id_transaksi"]).one()
            return hasil
        except Exception as e:
            print(f"kesalahan fucntion showTransaksiById: {e}")

    
    # method yang digunakan untuk menambah data ke tabel transaksi
    def insertTransaksi(self, **params):
        try:
            self.session.add(Transaksi(**params))  # tambah data
            self.session.commit()  # memberitahukan ke server
        except Exception as e:
            print(f"kesalahan function insertTransaksi: {e}")

    # method yang digunakan untuk mengubah data transaksi
    def updateDataById(self, **params):
        try:
            dataTransaksi = self.session.query(Transaksi).filter(Transaksi.id_transaksi == params["id_transaksi"]).one()  # ambil data user by id_user
            if "car_id" in params.keys():
                dataTransaksi.car_id = params["car_id"]
            self.session.commit()  # memberitahu server
        except Exception as e:
            print(f"kesalahan function updateDataById: {e}")

    # method yang digunakan untuk menghapus data transaksi by id_transaksi
    def deleteTransaksiById(self, **params):
        try:
            dataTransaksi = self.session.query(Transaksi).filter(Transaksi.id_transaksi == params["id_transaksi"]).one()
            self.session.delete(dataTransaksi)  # hapus data
            self.session.commit()  # memberitahu server
        except Exception as e:
            print(f"kesalahan fucntion deleteuserById: {e}")