from models.transaksi import Database
from flask import jsonify
import requests
import json

db = Database()

# function untuk melihat semua data di tabel transaksi
def viewAllTransaksi():
    try:
        hasil = db.showTransaksi()
        data_list = []
        if hasil is not None:
            for data in hasil:
                car_id = json.dumps({"id_car" : data.car_id})
                carDetails = getCarById(car_id)
                transaksi = {
                    "id_tranksi" : data.id_transaksi,
                    "user_id" : data.user_id,
                    "car_id" : data.car_id,
                    "nama_mobil" : carDetails["nama_mobil"],
                    "warna_mobil" : carDetails["warna_mobil"],
                    "harga_mobil" : carDetails["harga_mobil"]
                    }
                data_list.append(transaksi)
            return jsonify(data_list)
    except Exception as e:
        print(f"kesalahan function viewAllTransaksi: {e}")


# function untuk melihat satu data transaksi berdasarkan id_transaksi
def viewTransaksiById(**params):
    try:
        dbresult = db.showTransaksiById(**params)
        carDetail = getCarById(json.dumps({"id_car":dbresult.car_id}))
        data = {
            "id_transaksi" : dbresult.id_transaksi,
            "user_id" : dbresult.user_id,
            "car_id" : carDetail["id_car"],
            "nama_mobil" : carDetail["nama_mobil"],
            "warna_mobil" : carDetail["warna_mobil"],
            "harga_mobil" : carDetail["harga_mobil"]
        }
        return jsonify(data)
    except Exception as e:
        print(f"kesalahan function viewTransaksiById: {e}")


# function untuk menambah data ke tabel ransaksi
def tambahData(**params):
    try:
        db.insertTransaksi(**params)
        data = {
            "message" : "data berhasil ditambahkan"
        }
        return jsonify(data)
    except Exception as e:
        print(f"kesalahan function tambahData: {e}")


# function untuk mengubah data transaksi
def ubahDataById(**params):
    try:
        db.updateDataById(**params)
        data = {
            "message" : "data berhasil diubah"
        }
        return jsonify(data)
    except Exception as e:
        print(f"kesalahan function ubahData: {e}")


# function untuk menghapus data by id_transaksi
def hapusDataById(**params):
    try:
        db.deleteTransaksiById(**params)
        data = {
            "message" : "data berhasil dihapus"
        }
        return jsonify(data)
    except Exception as e:
        print(f"kesalahan function hapusDataById: {e}")


# functio untuk mendapatkan data car berdasarkan id_car
def getCarById(params):
    try:
        car_data = requests.get(
            url="http://localhost:8000/car",
            data = params
        )
        return json.loads(car_data.text)
    except Exception as e:
        print(f"kesalahan function getCarById: {e}")


# function untuk mendapatkan data user by user_id
def getUserById(**params):
    try:
        user_data = requests.get(
            url = "http://localhost:5000/user",
            data = params
        )
        data = user_data.json()
        return data
    except Exception as e:
        print(f"kesalahan function getUserById: {e}")

